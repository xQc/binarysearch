def addswap(tableau, valeur):
    tableau.append(valeur)
    i = len(tableau) - 1
    while tableau[i] < tableau[i - 1]:
        swapped = tableau[i - 1]
        tableau[i - 1] = tableau[i]
        tableau[i] = swapped
        i = i - 1
    print(tableau)
    return tableau
