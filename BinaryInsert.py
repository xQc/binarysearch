import math

def binary_insertion(tableau, valeur):
    iteration = 0
    # Cette condition permet de vérifier si le tableau est vide
    if len(tableau) != 0:
        imin = 0
        imax = len(tableau) - 1
        # Cette condition permet de vérifier le dernier élément puisqu'on doit utiliser une commande différente
        # pour "insérer" à la dernière position
        if tableau[imax] < valeur:
            iteration += 1
            tableau.append(valeur)
        else:
            # Algorithme de binary Search
            while imin < imax:
                iteration += 1
                milieu = math.floor((imin + imax) / 2)
                if tableau[milieu] < valeur:
                    imin = milieu + 1
                else:
                    imax = milieu
            tableau.insert(imax, valeur)
    else:
        iteration += 1
        tableau.append(valeur)

    print(tableau)
    print("Nombre d'itération : " + str(iteration))
    return tableau